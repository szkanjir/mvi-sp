%%
%% Created in 2018 by Martin Slapak
%%
%% Based on file for NRP report LaTeX class by Vit Zyka (2008)
%%
%% Compilation:
%% >pdflatex report
%% >bibtex report
%% >pdflatex report
%% >pdflatex report

\documentclass[english]{mvi-report}
\usepackage{subcaption}

\title{Real-time image denoising on OAK-D-Lite camera}

\author{Jiří Szkandera}
\affiliation{ČVUT - FIT}
\email{szkanjir@fit.cvut.cz}

\def\file#1{{\tt#1}}

\begin{document}

\maketitle

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Introduction}

The task was to create a neural network performing image denoising.
A convolutional autoencoder was used to perform the task.
Three variants of architecture were compared; without skip connections, with symmetric additive skip connections, and with concatenation on the output layer.
The image denoising was carried out in real-time on OAK-D-Lite camera

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{OAK-D-Lite}

OAK-D-Lite is a USB camera containing logic for AI machine vision tasks.
The abilities of the said camera are huge, but for the purposes of this paper, the following components are relevant.
First, a sensor used to capture RGB video is Sony IMX214.
Next, the camera contains a dedicated Intel Myriad X accelerator used for neural network inference.
There are 16 SHAVE cores on the accelerator, which can be thought of as mini GPUs.
Our neural network use 6 SHAVE cores, as the performance doesn't scale linearly with the number of cores, and 6 cores turned out to be enough.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Input data}

Small sRGB variant of Smartphone Image Denoising Dataset~\cite{abdelhamed2018high} was used as input data.
The size of the whole dataset is about 6.2~GB and contains 160 images with and without noise in high resolution (circa 5000x3000 px).

Data augmentation is applied to raw images in this order:
\begin{itemize}
	\item downscale to 800x800 px
	\item random rotation of \(\pm45\) degrees
	\item random horizontal flip
	\item random crop to 400x400 px
	\item random hue: convert the image to HSV and rotate the H component by a number in the interval \([-0.2, 0.2]\)
	\item random saturation: convert the image to HSV and multiply the S component by a number in the interval \([0.5, 1.5]\)
	\item add a gaussian noise with the mean of 0 and the standard deviation of 10/255
\end{itemize}
Using the described augmentation, 20 images were generated from every image in the original dataset (i.e. augmented dataset contains \(160\cdot 20 = 3200\) augmented images).
For every random operation, stateless RNG was used to achieve repeatability across runs.
Images were downscaled in order to match the capabilities of the Myriad chip.

\begin{figure*}[ht!]
	\includegraphics[width=\textwidth]{img/sidd.png}
	\caption{Noisy and denoised image comparison from SIDD dataset~\cite{abdelhamed2018high}.}
\end{figure*}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Used architectures}

Three variants of convolutional autoencoder were compared: base, with additive skip connections and with input concatenation on the output layer.

In every case, a network with two convolution layers (10 and 20 channels respectively) and three deconvolution layers (20, 10 and 3 channels respectively) was used.
Every layer uses a convolution window of size \(7\times 7\) per recommendation in~\cite{mao2016image}.
A batch normalization layer is used after convolution layers.
Neither pooling nor unpooling was used as it discards image details~\cite{mao2016image}.
The architecture was made shallow on purpose for it to run fast on the OAK.
ReLU was used as an activation function on hidden layers.
Sigmoid function was used on the output layer.
The architecture was inspired by the blueprint in the article~\cite{venkataraman2022image}

A base version uses described architecture as is without any modification.

In the additive case, additive skip connections were added symmetrically between convolution and deconvolution.
The reasoning behind this addition was that connections help with recovering image detail.
It also helps vanishing gradient, but our network isn't deep so it isn't a problem in our case.
The authors of this article~\cite{mao2016image} used similar architecture.

\begin{figure}[ht!]
	\includegraphics[width=0.5\textwidth]{img/symmetric-skip-connections.png}
	\caption{The architecture of a network with symmetric skip connections.}
\end{figure}

In the input concatenation case, the original image was concatenated to the last deconvolution input.
The reason for this addition is essentially the same as in the additive case.
A similar idea was used in PRIDNet~\cite{zhao2019pyramid}.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Training}

The neural networks were trained on a computer with Intel core i7 6700 CPU and Nvidia GTX 970 graphics card.
Adam optimizer was used to minimize mean squared error between images.
Root mean square error was used to track the error as its values can be interpreted with a grain of salt as an average distance between the value of the pixels.
The model was trained with a batch size of 10.
Early stopping with patience 10 was used and the models were trained until the early stopping criterion was reached.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Results and discussion}

During the training, we tracked RMSE of models on a validation set.
The output of the models is also compared on the test set~\ref{images:train} and on live output from the camera~\ref{images:oak}.
As we can see from figures \ref{graph:epochs} and \ref{graph:time}, additional connections have much better performance than the base model.
It was expected that the base model wasn't going to perform as well as the models with connections because hidden layers lose image detail.
This phenomenon can be seen visually in image comparison and is confirmed by the findings of the authors in~\cite{mao2016image}.

\begin{figure}[ht!]
	\includegraphics[width=0.5\textwidth]{img/epochs.pdf}
	\caption{Validation RMSE by epochs.}
	\label{graph:epochs}
\end{figure}

\begin{figure}[ht!]
	\includegraphics[width=0.5\textwidth]{img/time.pdf}
	\caption{Validation RMSE by time elapsed.}
	\label{graph:time}
\end{figure}

The base model successfully removed noise from the image.
However, much of the image detail and color information is lost during inference and as a result, the image appears blurry (this is especially noticeable when running on the OAK).
This is expected, as the traditional procedure for removing image noise is to use blurring filters (e.g. Gaussian filter).
Also, the hue of the image is slightly changed.
This could be helped by converting an image to HSV color space before inference.
However, the conversion isn't available on the OAK-D-Lite.

The concatenative model trained much faster than the other models and was able to remove a color component of the noise from the image.
The sharpness of the image is preserved and even enhanced, which is especially noticeable in the lower part of the image on the live output.
However, texture component of the noise remained on the images.
The processed images are also darkened.
This could be helped by converting images to HSV if available.

The additive model achieved by far the best visual result.
Although it trained much longer than the concatenative model, it was able to provide the most visually pleasing result.
Interestingly, the additive model doesn't do much in terms of removing noise on the camera images, which is a good thing as there isn't much noise to begin with.

\begin{figure*}[ht!]
	\centering
	\begin{tabular}{cc}
		\subfloat[Original image.]{
			\includegraphics[width=0.45\textwidth]{img/train/gold_noisy.png}
		} &
		\subfloat[Base model applied.]{
			\includegraphics[width=0.45\textwidth]{img/train/base.png}
		} \\
		\subfloat[Additive model applied.]{
			\includegraphics[width=0.45\textwidth]{img/train/add.png}
		} &
		\subfloat[Concatenative model applied.]{
			\includegraphics[width=0.45\textwidth]{img/train/concat.png}
		}
	\end{tabular}
	\caption{Visual comparison of different models on the test set.}
	\label{images:train}
\end{figure*}

\begin{figure*}[ht!]
	\centering
	\begin{tabular}{cc}
		\subfloat[Original image.]{
			\includegraphics[width=0.45\textwidth]{img/oak/gold.png}
		} &
		\subfloat[Base model applied.]{
			\includegraphics[width=0.45\textwidth]{img/oak/base.png}
		} \\
		\subfloat[Additive model applied.]{
			\includegraphics[width=0.45\textwidth]{img/oak/add.png}
		} &
		\subfloat[Concatenative model applied.]{
			\includegraphics[width=0.45\textwidth]{img/oak/concat.png}
		}
	\end{tabular}
	\caption{Visual comparison of different models running on OAK-D-Lite camera.}
	\label{images:oak}
\end{figure*}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Summary}

In summary, this paper found that the autoencoder CNN was able to effectively remove noise from the images captured by the OAK-D-Lite camera, resulting in a significant improvement in image quality.
This was especially noticeable in images with high levels of noise, where the denoising process was able to effectively restore the original features of the image.

One major challenge encountered when running real-time neural network inference on the OAK-D-Lite camera was the weak chip used in the device.
The Myriad X is a relatively low-powered chip, which can limit the resolution of the image processed by the neural network.

By itself, Sony IMX214 sensor was able to capture an image containing fairly low levels of noise in challenging low light conditions without any postprocessing.
A more diverse dataset containing images captured with IMX214 sensor in various lightning conditions would be helpful.

The results of this paper are useful for didactic and showcase purposes for the Improlab laboratory.
The paper provides a clear demonstration of the effectiveness of an autoencoder CNN for image denoising, with the results showing a significant improvement in image quality after the denoising process.
Also, the paper demonstrates the ability of the OAK-D-Lite to effectively run a neural network in real-time.
This showcases the potential of the OAK-D-Lite for applications requiring real-time image processing.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\bibliography{reference}

\end{document}
