import os
import sys
import urllib.request
import zipfile
import shutil
from pathlib import Path

import numpy as np
import tensorflow as tf
import tensorflow_addons as tfa
from common import imwrite, IMG_SHAPE


class SIDD:
    _URL = 'https://competitions.codalab.org/my/datasets/download/a26784fe-cf33-48c2-b61f-94b299dbc0f2'
    _NAME = 'SIDD_Small_sRGB'

    def download(self, args):
        dataset_dir = Path(args.dataset_dir)
        dataset_dir.mkdir(exist_ok=True)

        path_zip = dataset_dir / f'{self._NAME}.zip'
        if not os.path.exists(path_zip):
            print(f'Downloading dataset {self._NAME}...', file=sys.stderr)
            urllib.request.urlretrieve(self._URL, filename=path_zip)

        dir_unzipped = dataset_dir / self._NAME
        if not os.path.exists(dir_unzipped):
            print(f'Unzipping dataset {self._NAME}...', file=sys.stderr)
            with zipfile.ZipFile(path_zip, 'r') as zip_file:
                zip_file.extractall(dir_unzipped)

        if not os.path.exists(self.dir_noisy) or not os.path.exists(self.dir_denoised):
            print('Restructuring dataset folder...', file=sys.stderr)
            self.dir_noisy.mkdir(exist_ok=True)
            self.dir_denoised.mkdir(exist_ok=True)
            (dir_unzipped / 'SIDD_Small_sRGB_Only' / '_ReadMe.txt').rename(dir_unzipped / 'README.txt')

            for dir_scene in (dir_unzipped / 'SIDD_Small_sRGB_Only' / 'Data').iterdir():
                scene = dir_scene.name
                for file_img in dir_scene.iterdir():
                    img = file_img.name
                    if img.startswith('GT_'):
                        file_img.rename(self.dir_denoised / f'{scene}_{img}')
                    elif img.startswith('NOISY_'):
                        file_img.rename(self.dir_noisy / f'{scene}_{img}')
                    else:
                        print(f'Warning: unknown prefix in {img}')

            shutil.rmtree(dir_unzipped / 'SIDD_Small_sRGB_Only')

    def __init__(self, args, test_size):
        self.dir_noisy = Path(args.dataset_dir) / self._NAME / 'noisy'
        self.dir_denoised = Path(args.dataset_dir) / self._NAME / 'denoised'

        self.download(args)

        ds_noisy = tf.keras.preprocessing.image_dataset_from_directory(
            self.dir_noisy,
            label_mode=None,
            image_size=(2*IMG_SHAPE['H'], 2*IMG_SHAPE['W']),
            shuffle=False,
            batch_size=None,
        )
        ds_denoised = tf.keras.preprocessing.image_dataset_from_directory(
            self.dir_denoised,
            label_mode=None,
            image_size=(2*IMG_SHAPE['H'], 2*IMG_SHAPE['W']),
            shuffle=False,
            batch_size=None,
        )

        def normalize(img_noisy, img_denoised):
            return img_noisy/255, img_denoised/255

        ds = (
            tf.data.Dataset.zip((ds_noisy, ds_denoised))
            .shuffle(100, seed=args.seed)
            .map(normalize)
            .prefetch(tf.data.AUTOTUNE)  # pusti se paralelne
        )

        def add_gaussian_noise(img, seed, *args, **kwargs):
            noise = tf.random.stateless_normal(shape=tf.shape(img), seed=seed, *args, **kwargs, dtype=tf.float32)
            return tf.clip_by_value(img + noise, 0.0, 1.0)

        def augment(imgs, seed):
            img_noisy, img_denoised = imgs

            imgs_aug_noisy, imgs_aug_denoised = [], []
            for i in range(20):
                tf_image_seed = (seed, i)

                # Random rotation
                angle = tf.random.stateless_uniform([], seed=tf_image_seed, minval=-np.pi/4, maxval=np.pi/4)
                img_aug_noisy = tfa.image.rotate(img_noisy, angle, interpolation='bilinear', fill_mode='wrap')
                img_aug_denoised = tfa.image.rotate(img_denoised, angle, interpolation='bilinear', fill_mode='wrap')

                # Random horizontal flip
                img_aug_noisy = tf.image.stateless_random_flip_left_right(img_aug_noisy, seed=tf_image_seed)
                img_aug_denoised = tf.image.stateless_random_flip_left_right(img_aug_denoised, seed=tf_image_seed)

                # Random crop
                img_aug_noisy = tf.image.stateless_random_crop(
                    img_aug_noisy, size=(IMG_SHAPE['H'], IMG_SHAPE['W'], IMG_SHAPE['C']), seed=tf_image_seed)
                img_aug_denoised = tf.image.stateless_random_crop(
                    img_aug_denoised, size=(IMG_SHAPE['H'], IMG_SHAPE['W'], IMG_SHAPE['C']), seed=tf_image_seed)

                # Random hue
                img_aug_noisy = tf.image.stateless_random_hue(
                    img_aug_noisy, 0.2, seed=tf_image_seed)
                img_aug_denoised = tf.image.stateless_random_hue(
                    img_aug_denoised, 0.2, seed=tf_image_seed)

                # Random saturation
                img_aug_noisy = tf.image.stateless_random_saturation(
                    img_aug_noisy, 0.5, 1.5, seed=tf_image_seed)
                img_aug_denoised = tf.image.stateless_random_saturation(
                    img_aug_denoised, 0.5, 1.5, seed=tf_image_seed)

                # Add aditional noise
                img_aug_noisy = add_gaussian_noise(img_aug_noisy, tf_image_seed, mean=0, stddev=10/255)

                # Append
                imgs_aug_noisy.append(img_aug_noisy)
                imgs_aug_denoised.append(img_aug_denoised)

            return imgs_aug_noisy, imgs_aug_denoised

        def resize(img_noisy, img_denoised):
            return (
                tf.image.resize(img_noisy, (IMG_SHAPE['H'], IMG_SHAPE['W'])),
                tf.image.resize(img_denoised, (IMG_SHAPE['H'], IMG_SHAPE['W'])),
            )

        def save(imgs):
            img_noisy, img_denoised = next(iter(imgs))
            output_dir = Path(args.output_dir)
            for i, (img_aug_noisy, img_aug_denoised) in enumerate(zip(*augment((img_noisy, img_denoised), 0))):
                imwrite(output_dir / 'augment' / f'{i:03}_aug_noisy.png', img_aug_noisy.numpy())
                imwrite(output_dir / 'augment' / f'{i:03}_aug_denoised.png', img_aug_denoised.numpy())

        save(ds.take(1))

        self.ds_test = (
            ds.take(test_size)
            .map(resize)
            .batch(args.batch_size)
            .prefetch(tf.data.AUTOTUNE)
        )

        counter = tf.data.experimental.Counter()
        self.ds_train = (
            tf.data.Dataset.zip((ds.skip(test_size), counter))
            .map(augment)
            .unbatch()
            .shuffle(200, seed=args.seed)
            .batch(args.batch_size)
            .prefetch(tf.data.AUTOTUNE)
        )

    @property
    def train(self):
        return self.ds_train

    @property
    def test(self):
        return self.ds_test
