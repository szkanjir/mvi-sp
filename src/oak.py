from msg_sync import MsgSync
import numpy as np
import cv2 as cv
import depthai as dai
import argparse
from pathlib import Path
import datetime
import time
from common import imwrite, IMG_SHAPE
from collections import deque
import itertools

parser = argparse.ArgumentParser()
parser.add_argument('--model', required=True, type=str, help='A path to a model blob.')
parser.add_argument('--output_dir', default='./output/', type=str, help='Directory containing output images.')
parser.add_argument('--mode', type=str, required=True, choices=['add', 'base', 'concat'], help='Which model to operate on.')


def average_fps(times):
    diffs = []
    for x, y in itertools.pairwise(times):
        diffs.append(y - x)
    if len(diffs) == 0:
        return 0
    average_diff = np.average(diffs)
    return 1 / average_diff


def print_system_info(info, times):
    # CSS: CPU SubSystem (main cores)
    # MSS: Media SubSystem
    # UPA: Microprocessor(UP) Array – Shaves
    # DSS: DDR SubSystem
    # CMX: Connection MatriX (CMX) block (Shave RAM)

    m = 1024 * 1024  # MiB
    print(f'Average fps: {average_fps(times):.2f}')
    print(f'Ddr used / total: {info.ddrMemoryUsage.used / m:.2f} / {info.ddrMemoryUsage.total / m:.2f} MiB')
    print(f'Cmx used / total: {info.cmxMemoryUsage.used / m:.2f} / {info.cmxMemoryUsage.total / m:.2f} MiB')
    print(f'LeonCss heap used / total: {info.leonCssMemoryUsage.used / m:.2f} / {info.leonCssMemoryUsage.total / m:.2f} MiB')
    print(f'LeonMss heap used / total: {info.leonMssMemoryUsage.used / m:.2f} / {info.leonMssMemoryUsage.total / m:.2f} MiB')
    t = info.chipTemperature
    print(f'Chip temperature - average: {t.average:.2f}, css: {t.css:.2f}, mss: {t.mss:.2f}, upa: {t.upa:.2f}, dss: {t.dss:.2f}')
    print(f'Cpu usage - Leon CSS: {info.leonCssCpuUsage.average * 100:.2f}%, Leon MSS: {info.leonMssCpuUsage.average * 100:.2f} %')
    print('----------------------------------------')


def uint8_to_float32(frame):
    return np.clip(frame.astype(np.float32)/255, 0, 255)


def get_output_layer_name(args):
    if args.mode == 'concat':
        return 'model/output_conv2d/Sigmoid'
    elif args.mode == 'add':
        return 'model/sigmoid0/Sigmoid'
    elif args.mode == 'base':
        return 'model/output/Sigmoid'
    else:
        raise ValueError(f'Invalid mode {args.mode}')


def main(args: argparse.Namespace):
    pipeline = dai.Pipeline()

    rgb = pipeline.create(dai.node.ColorCamera)
    rgb.setPreviewSize(IMG_SHAPE['W'], IMG_SHAPE['H'])
    rgb.setInterleaved(False)
    rgb.setColorOrder(dai.ColorCameraProperties.ColorOrder.RGB)

    nn = pipeline.create(dai.node.NeuralNetwork)
    nn.setBlobPath(args.model)
    nn.setNumInferenceThreads(2)

    logger = pipeline.create(dai.node.SystemLogger)
    logger.setRate(1)  # 1 Hz

    xout_rgb = pipeline.create(dai.node.XLinkOut)
    xout_rgb.setStreamName('rgb')
    xout_nn = pipeline.create(dai.node.XLinkOut)
    xout_nn.setStreamName('nn')
    xout_logger = pipeline.create(dai.node.XLinkOut)
    xout_logger.setStreamName('sysinfo')

    rgb.preview.link(nn.input)
    rgb.preview.link(xout_rgb.input)
    nn.out.link(xout_nn.input)
    logger.out.link(xout_logger.input)

    times = deque([time.time()], maxlen=15)
    with dai.Device(pipeline) as device:
        sync = MsgSync(['rgb', 'nn'])
        queues = {}
        for name in ['rgb', 'nn', 'sysinfo']:
            queues[name] = device.getOutputQueue(name, maxSize=4, blocking=False)

        frame_merged = None
        while True:
            for name in ['rgb', 'nn']:
                sync.add_msg(queues[name].tryGet(), name)

            msgs = sync.get_msgs()
            if msgs is not None:
                frame_rgb = msgs['rgb'].getCvFrame()
                frame_nn = (
                    np.asarray(msgs['nn'].getLayerFp16(get_output_layer_name(args)))
                    .reshape(3, IMG_SHAPE['W'], IMG_SHAPE['H'])
                    .transpose((1, 2, 0))
                )

                frame_merged = np.hstack((uint8_to_float32(frame_rgb), frame_nn))
                cv.imshow('preview', cv.resize(frame_merged, (1600, 800)))

                times.append(time.time())

            msg_sysinfo = queues['sysinfo'].tryGet()
            if msg_sysinfo is not None:
                print_system_info(msg_sysinfo, times)

            pressed_key = cv.waitKey(1)
            if pressed_key == ord('q'):
                break
            elif pressed_key == ord('s') and frame_merged is not None:
                imwrite(Path(args.output_dir) / 'screenshots' /
                        f'{datetime.datetime.now().strftime("%Y-%m-%d_%H%M%S")}.png', frame_merged)


if __name__ == '__main__':
    args = parser.parse_args([] if '__file__' not in globals() else None)
    main(args)
