class MsgSync:
    def __init__(self, names):
        self.msgs = {}
        self.names = set(names)

    def add_msg(self, msg, name):
        if msg is None:
            return

        seq = msg.getSequenceNum()

        if seq not in self.msgs:
            self.msgs[seq] = {}

        if name in self.names:
            self.msgs[seq][name] = msg

    def get_msgs(self):
        seq_remove = []

        for seq, msgs in sorted(self.msgs.items(), key=lambda x: x[0]):
            seq_remove.append(seq)

            if self.names.issubset(msgs):
                for seq in seq_remove:
                    del self.msgs[seq]
                return msgs
        return None
