from pathlib import Path
import numpy as np
import os
import cv2 as cv


IMG_SHAPE = {
    'W': 400,
    'H': 400,
    'C': 3,
}

def imwrite(path, img):
    Path(os.path.dirname(path)).mkdir(exist_ok=True, parents=True)
    cv.imwrite(str(path), (img * 255).astype(np.uint8))
