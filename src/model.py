import os
import datetime
import argparse
import re
from pathlib import Path
from sidd import SIDD

import blobconverter
import numpy as np
import tensorflow as tf
from tensorflow.python.framework.convert_to_constants import convert_variables_to_constants_v2
from common import imwrite, IMG_SHAPE

os.environ.setdefault('TF_CPP_MIN_LOG_LEVEL', '2')  # Report only TF errors by default

parser = argparse.ArgumentParser()
parser.add_argument('--threads', default=0, type=int, help='Maximum number of threads to use.')
parser.add_argument('--seed', default=42, type=int, help='Random seed.')
parser.add_argument('--dataset_dir', default='./dataset/', type=str, help='Directory containing images.')
parser.add_argument('--model_dir', default='./models/', type=str, help='Directory containing models.')
parser.add_argument('--output_dir', default='./output/', type=str, help='Directory containing output images.')
parser.add_argument('--batch_size', default=10, type=int, help='Batch size.')
parser.add_argument('--epochs', default=100, type=int, help='Number of epochs.')
parser.add_argument('--freeze', default=False, action='store_true', help='Freeze the latest model.')
parser.add_argument('--from_checkpoint', default=False, action='store_true', help='Continue training a previous model.')
parser.add_argument('--mode', type=str, required=True, choices=['add', 'base', 'concat'], help='Which model to operate on.')


def create_model_add():
    inputs = tf.keras.layers.Input(shape=(IMG_SHAPE['H'], IMG_SHAPE['W'], IMG_SHAPE['C']), name='input')

    # Encoder
    encode1 = tf.keras.layers.Conv2D(10, (7, 7), activation='relu', padding='same', name='encode1_conv2d')(inputs)
    encode2 = tf.keras.layers.Conv2D(20, (7, 7), activation=None, padding='same', use_bias=False, name='encode2_conv2d')(encode1)
    encode2 = tf.keras.layers.BatchNormalization(name='encode2_batch_normalization')(encode2)
    encode2 = tf.keras.layers.ReLU(name='encode2_re_lu')(encode2)

    # Decoder
    decode2 = tf.keras.layers.Conv2DTranspose(20, (7, 7), activation='relu', padding='same', name='decode2_conv2d')(encode2)
    decode1 = tf.keras.layers.Conv2DTranspose(10, (7, 7), activation=None, padding='same', name='decode1_conv2d')(decode2)
    add1 = tf.keras.layers.Add(name='add1')([decode1, encode1])
    relu1 = tf.keras.layers.ReLU(name='relu1')(add1)
    decode0 = tf.keras.layers.Conv2DTranspose(
        IMG_SHAPE['C'], (7, 7), activation=None, padding='same', name='output_conv2d')(relu1)
    add0 = tf.keras.layers.Add(name='add0')([decode0, inputs])
    sigmoid0 = tf.keras.layers.Activation(activation='sigmoid', name='sigmoid0')(add0)
    return tf.keras.Model(inputs=inputs, outputs=sigmoid0)


def create_model_concat():
    inputs = tf.keras.layers.Input(shape=(IMG_SHAPE['H'], IMG_SHAPE['W'], IMG_SHAPE['C']), name='input')

    # Encoder
    encode1 = tf.keras.layers.Conv2D(10, (7, 7), activation='relu', padding='same', name='encode1_conv2d')(inputs)
    encode2 = tf.keras.layers.Conv2D(20, (7, 7), activation=None, padding='same',
                                     use_bias=False, name='encode2_conv2d')(encode1)
    encode2 = tf.keras.layers.BatchNormalization(name='encode2_batch_normalization')(encode2)
    encode2 = tf.keras.layers.ReLU(name='encode2_re_lu')(encode2)

    # Decoder
    decode2 = tf.keras.layers.Conv2DTranspose(20, (7, 7), activation='relu', padding='same', name='decode2_conv2d')(encode2)
    decode1 = tf.keras.layers.Conv2DTranspose(10, (7, 7), activation='relu', padding='same', name='decode1_conv2d')(decode2)
    concat = tf.keras.layers.Concatenate(name='concat')([decode1, inputs])
    outputs = tf.keras.layers.Conv2DTranspose(
        IMG_SHAPE['C'], (7, 7), activation='sigmoid', padding='same', name='output_conv2d')(concat)
    return tf.keras.Model(inputs=inputs, outputs=outputs)


def create_model_base():
    inputs = tf.keras.layers.Input(shape=(IMG_SHAPE['H'], IMG_SHAPE['W'], IMG_SHAPE['C']), name='input')

    # Encoder
    encode = tf.keras.layers.Conv2D(10, (7, 7), activation='relu', padding='same')(inputs)
    encode = tf.keras.layers.Conv2D(20, (7, 7), activation=None, padding='same', use_bias=False)(encode)
    encode = tf.keras.layers.BatchNormalization()(encode)
    encode = tf.keras.layers.ReLU()(encode)

    # Decoder
    decode = tf.keras.layers.Conv2DTranspose(20, (7, 7), activation='relu', padding='same')(encode)
    decode = tf.keras.layers.Conv2DTranspose(10, (7, 7), activation='relu', padding='same')(decode)

    outputs = tf.keras.layers.Conv2DTranspose(
        IMG_SHAPE['C'], (7, 7), activation='sigmoid', padding='same', name='output')(decode)
    return tf.keras.Model(inputs=inputs, outputs=outputs)


def create_model(args):
    if args.mode == 'add':
        return create_model_add()
    elif args.mode == 'base':
        return create_model_base()
    elif args.mode == 'concat':
        return create_model_concat()
    else:
        raise ValueError(f'Invalid mode {args.mode}')


def freeze_graph(args: argparse.Namespace):
    # Load previous model
    checkpoint_path = tf.train.latest_checkpoint(args.model_dir)
    model = create_model(args)
    model.load_weights(checkpoint_path).expect_partial()

    # Convert Keras model to ConcreteFunction
    full_model = tf.function(lambda inputs: model(inputs))
    full_model = full_model.get_concrete_function(
        [tf.TensorSpec(model_input.shape, model_input.dtype) for model_input in model.inputs])

    # Get frozen ConcreteFunction
    frozen_func = convert_variables_to_constants_v2(full_model)
    frozen_func.graph.as_graph_def()

    # Save frozen graph from frozen ConcreteFunction
    frozen_pb_path = str(checkpoint_path) + '.pb'
    tf.io.write_graph(graph_or_graph_def=frozen_func.graph, logdir=os.path.dirname(frozen_pb_path),
                      name=os.path.basename(frozen_pb_path), as_text=False)

    return frozen_pb_path


def convert_to_blob(args: argparse.Namespace, frozen_pb_path):
    return blobconverter.from_tf(
        frozen_pb=frozen_pb_path,
        data_type='FP16',
        shaves=6,
        output_dir=args.model_dir,
        optimizer_params=[
            '--reverse_input_channels',  # RGB <--> BGR conversion
            f'--input_shape=[1,{IMG_SHAPE["H"]},{IMG_SHAPE["W"]},{IMG_SHAPE["C"]}]',
            '--mean_values=[0,0,0]',
            '--scale_values=[255,255,255]',
        ],
    )


def train(args: argparse.Namespace):
    np.random.seed(args.seed)
    tf.random.set_seed(args.seed)
    tf.keras.utils.set_random_seed(args.seed)
    tf.config.threading.set_inter_op_parallelism_threads(0)
    tf.config.threading.set_intra_op_parallelism_threads(0)

    # Create logdir name
    logdir = os.path.join('logs', '{}-{}-{}'.format(
        os.path.basename(globals().get('__file__', 'notebook')),
        datetime.datetime.now().strftime('%Y-%m-%d_%H%M%S'),
        ','.join(('{}={}'.format(re.sub('(.)[^_]*_?', r'\1', k), str(v).replace('/', '-'))
                  for k, v in sorted(vars(args).items())))
    ))

    ds = SIDD(args, 20)

    if args.from_checkpoint:
        checkpoint_path = tf.train.latest_checkpoint(args.model_dir)
        model = create_model(args)
        model.load_weights(checkpoint_path)
    else:
        model = create_model(args)

    model.summary()
    model.compile(
        optimizer=tf.optimizers.Adam(),
        loss=tf.losses.MeanSquaredError(),
        metrics=[tf.metrics.RootMeanSquaredError()]
    )

    tensorboard_cb = tf.keras.callbacks.TensorBoard(logdir, histogram_freq=1)
    tensorboard_cb._close_writers = lambda: None  # A hack allowing to keep the writers open.
    early_stopping_cb = tf.keras.callbacks.EarlyStopping(monitor='loss', patience=10)

    checkpoint_path = Path(args.model_dir) / (
        'ckpt-' + datetime.datetime.now().strftime('%Y-%m-%d_%H%M%S') + '-{epoch:04d}')
    checkpoint_cb = tf.keras.callbacks.ModelCheckpoint(
        filepath=checkpoint_path, verbose=1, save_weights_only=True, save_freq='epoch')

    def evaluate_test(epoch, logs):
        if epoch + 1 == args.epochs:
            test_logs = model.evaluate(
                ds.test, batch_size=args.batch_size, return_dict=True, verbose=0,
            )
            logs.update({'val_test_' + name: value for name, value in test_logs.items()})

    evaluate_test_cb = tf.keras.callbacks.LambdaCallback(on_epoch_end=evaluate_test)

    model.fit(
        ds.train,
        epochs=args.epochs,
        shuffle=False,
        callbacks=[tensorboard_cb, early_stopping_cb, checkpoint_cb, evaluate_test_cb],
    )

    def evaluate_test_imgs():
        i = 0
        for batch_test in ds.test:
            batch_predict = model.predict(batch_test[0])
            output_dir = Path(args.output_dir)
            for img_noisy, img_denoised, img_predict in zip(batch_test[0], batch_test[1], batch_predict):
                imwrite(output_dir / 'test' / f'{i:03}_noisy.png', img_noisy.numpy())
                imwrite(output_dir / 'test' / f'{i:03}_denoised.png', img_denoised.numpy())
                imwrite(output_dir / 'test' / f'{i:03}_predict.png', img_predict)
                i += 1

    evaluate_test_imgs()


def main(args: argparse.Namespace):
    if args.freeze:
        frozen_pb_path = freeze_graph(args)
        convert_to_blob(args, frozen_pb_path)
    else:
        train(args)


if __name__ == '__main__':
    args = parser.parse_args([] if '__file__' not in globals() else None)
    main(args)
